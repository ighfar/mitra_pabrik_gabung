import Vue from 'vue'
import Router from 'vue-router'
import { normalizeURL, decode } from 'ufo'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _c09ce990 = () => interopDefault(import('..\\pages\\home.vue' /* webpackChunkName: "pages/home" */))
const _11d12eff = () => interopDefault(import('..\\pages\\home\\artikels.vue' /* webpackChunkName: "pages/home/artikels" */))
const _d808de8a = () => interopDefault(import('..\\pages\\home\\assets.vue' /* webpackChunkName: "pages/home/assets" */))
const _2679244d = () => interopDefault(import('..\\pages\\home\\customers.vue' /* webpackChunkName: "pages/home/customers" */))
const _7b865b0c = () => interopDefault(import('..\\pages\\home\\dashboard.vue' /* webpackChunkName: "pages/home/dashboard" */))
const _4fb64351 = () => interopDefault(import('..\\pages\\home\\events.vue' /* webpackChunkName: "pages/home/events" */))
const _3b051862 = () => interopDefault(import('..\\pages\\home\\mutations.vue' /* webpackChunkName: "pages/home/mutations" */))
const _74d48fa7 = () => interopDefault(import('..\\pages\\home\\mutations\\artikel-form.vue' /* webpackChunkName: "pages/home/mutations/artikel-form" */))
const _73e20d79 = () => interopDefault(import('..\\pages\\home\\mutations\\event-form.vue' /* webpackChunkName: "pages/home/mutations/event-form" */))
const _01699fa7 = () => interopDefault(import('..\\pages\\home\\mutations\\showroom-form.vue' /* webpackChunkName: "pages/home/mutations/showroom-form" */))
const _a52f952e = () => interopDefault(import('..\\pages\\home\\scan-qr.vue' /* webpackChunkName: "pages/home/scan-qr" */))
const _0e9f3810 = () => interopDefault(import('..\\pages\\home\\showroom.vue' /* webpackChunkName: "pages/home/showroom" */))
const _86cdb966 = () => interopDefault(import('..\\pages\\home\\event\\rsvp.vue' /* webpackChunkName: "pages/home/event/rsvp" */))
const _65449223 = () => interopDefault(import('..\\pages\\logout.vue' /* webpackChunkName: "pages/logout" */))
const _aa85c16e = () => interopDefault(import('..\\pages\\index.vue' /* webpackChunkName: "pages/index" */))

const emptyFn = () => {}

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: '/',
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/home",
    component: _c09ce990,
    name: "home",
    children: [{
      path: "artikels",
      component: _11d12eff,
      name: "home-artikels"
    }, {
      path: "assets",
      component: _d808de8a,
      name: "home-assets"
    }, {
      path: "customers",
      component: _2679244d,
      name: "home-customers"
    }, {
      path: "dashboard",
      component: _7b865b0c,
      name: "home-dashboard"
    }, {
      path: "events",
      component: _4fb64351,
      name: "home-events"
    }, {
      path: "mutations",
      component: _3b051862,
      name: "home-mutations",
      children: [{
        path: "artikel-form",
        component: _74d48fa7,
        name: "home-mutations-artikel-form"
      }, {
        path: "event-form",
        component: _73e20d79,
        name: "home-mutations-event-form"
      }, {
        path: "showroom-form",
        component: _01699fa7,
        name: "home-mutations-showroom-form"
      }]
    }, {
      path: "scan-qr",
      component: _a52f952e,
      name: "home-scan-qr"
    }, {
      path: "showroom",
      component: _0e9f3810,
      name: "home-showroom"
    }, {
      path: "event/rsvp",
      component: _86cdb966,
      name: "home-event-rsvp"
    }]
  }, {
    path: "/logout",
    component: _65449223,
    name: "logout"
  }, {
    path: "/",
    component: _aa85c16e,
    name: "index"
  }],

  fallback: false
}

export function createRouter (ssrContext, config) {
  const base = (config._app && config._app.basePath) || routerOptions.base
  const router = new Router({ ...routerOptions, base  })

  // TODO: remove in Nuxt 3
  const originalPush = router.push
  router.push = function push (location, onComplete = emptyFn, onAbort) {
    return originalPush.call(this, location, onComplete, onAbort)
  }

  const resolve = router.resolve.bind(router)
  router.resolve = (to, current, append) => {
    if (typeof to === 'string') {
      to = normalizeURL(to)
    }
    return resolve(to, current, append)
  }

  return router
}
