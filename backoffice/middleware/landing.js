export default async ({redirect, store, app}) => {
    const checkToken = app.$cookies.get('admin-token') ? true : false
    if (checkToken) {
        redirect({name: 'home'})
    }
}