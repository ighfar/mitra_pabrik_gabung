import axios from 'axios'

export default async ({app, redirect, store}) => {
    console.log('auth jalan')
    const checkStore = store.state.auth.id ? true : false
    console.log("ada di store: " + checkStore)
    if (!checkStore) {
        if(app.$cookies.get('admin-token')) {
            await axios.post(`${process.env.API_URL}/user/admin/check-token`,{},{
                headers: {
                    "auth-token":app.$cookies.get('admin-token')
                }
            }).then(res => {
                let dataLogin = res.data.user
                store.state.auth.id_admin = dataLogin.id_admin
                store.state.auth.level = dataLogin.level
                store.state.auth.nama = dataLogin.nama
                console.log("auth aman")
            }).catch(err => {
                console.log("redirect ke logout")
                redirect({name: 'logout'})
            })
        } else {
            console.log("redirect ke login kareng ad token")
            redirect({name: 'logout'})
        }
    }
}